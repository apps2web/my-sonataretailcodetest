var global = {};
var imports = {
  express: require('express'),
  UserRouters: require('./lib/users/routers/user.routers')
};

(function() {
  var app = function(options) {
    this.initialize(options);
  };
  app.prototype = {
    initialize: function(imports) {
      this.imports = imports;
      this.express = this.imports.express();
      this.routers();

    },
    routers: function() {
      this.userRouters = new this.imports.UserRouters(this.express);
    },
    listen: function(port) {
      this.express.listen(port);
    }
  };
  global.server = app;
})();

var server = new global.server(imports);

server.listen(4321);
