Sonata retail code test
=======================

Hello, fellow javascripter!

If you are reading this you probably have thought about becoming part of our small but awesome team ... So good luck with the test!


Conditions
==========
There's no time limits to complete the test. We're not going to add any hidden code to monitor your first test server request to check when you start.  We're coders too and we have made code tests too, and we know how it works and how it feel. So take your time, seriously it's ok if you want to take a whole weekend to complete the test!


This code test is designed to take no more than a couple of hours, less for someone with good javascript skills. Basically we would like to know if you're able to produce simple and elegant code, how you test you're code, if you're able to code both server and client side avoiding common problems and what's you're code style (comments, structure, organization).


Of course if you send us the solution after just 30 minutes we'll be impressed as hell and not so impressed if you answer in a week, but please keep in mind that we prefer a nice solution that took you a day than a dirty one that took you an hour.


The problem
===========

The tests is composed by two different parts for evaluating both you're sever and client side coding skills..

On the client side:
-------------------
You need to build a single page application that display the current hour, minute and second in real time. The result should look like this:


![clock](http://upload.wikimedia.org/wikipedia/commons/b/be/Digital.gif);

Well in HH:MM:SS format...we didn't find a better animated gif on google images :)

The displayed time should be the same that the clock of the computer who displaying the site.

On the server side:
-------------------
 In this repo you'll find a very basic server. The server has an entry point (http://localhost/user/:userId) that returns a single integer between -10 and 10 representing the timezone of the user selected by :userId (mus be an integer, the dumb server accepts just numbers).

What you need to do is code another entry point with format:

http://localhost/currentTime/:timeZone

which return a json with the server time converted to the timezone passed as parameter (-10 to 10). For making it easier start from the assumption that the server is always on the timezone 0, so you can just assume that the -10 timezone is just 10 hours less than the server (eg: server is at 14:00 timezone -10 is at 04:00).

Once done the client side code must be updated including an html input that allows the user to manually enter a userId. If an userId is entered in the new input, the web should display the hour of the timezone associated with that user. The screen clock should use the entry point developed by you as time origin instead the local computer clock.

What we want
============
- Object oriented code
- Unit tests
- Being able to adapt your code style to the project
- Develop the client side code without the use of any external library / module. You don't need it make it crossbrowser, just tell us "I have tested it on Chrome (or Firefox)" and we'll test it there.


Bonus points
============
- Make the clock analog (analog rocks!)
- Don't use ajax/long polling for client/servehtml r com
