var imports = {
  Q: require('q'),
};

var Q = imports.Q;

(function () {
  var UserService = function () {
  };

  UserService.prototype = {
    getUserTimeZone: function(userId) {
      var dfd = Q.defer();
      setTimeout(function(){
        zone = (userId % 20) - 10;
        dfd.resolve(zone)
      }, Math.floor(Math.random() * 5))
      return dfd.promise;
    }
  }

  module.exports = UserService;
})();
